#include "timer.h"

void timer::start()
{
	start_marker = chrono::system_clock::now();
	last_time_marker = start_marker;
}

chrono::duration<double> timer::get_duration(bool total)
{
	chrono::time_point<chrono::system_clock> cur_marker = chrono::system_clock::now();
	chrono::duration<double> res;
	
	res = total ? cur_marker - start_marker : cur_marker - last_time_marker;
	// Do not move time marker in case of total request
	last_time_marker = total ? last_time_marker : cur_marker;
	return res;
}

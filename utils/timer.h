#ifndef TIMER_H
#define TIMER_H

#include <iostream>
#include <chrono>
#include <ctime>

using namespace std;

class timer final
{
private:
	chrono::time_point<chrono::system_clock> start_marker;
	chrono::time_point<chrono::system_clock> last_time_marker;
	
public:
	void start();
	
	chrono::duration<double> get_duration(bool total);
};


#endif // TIMER_H

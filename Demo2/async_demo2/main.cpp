#include <iostream>
#include <fstream>
#include <thread>
#include <future>
#include <string>
#include <algorithm>
#include <boost/filesystem.hpp>

#include <fcntl.h>
#include <sys/stat.h>
#include <archive.h>
#include <archive_entry.h>

#ifdef MEASURE_TIME
#include "timer.h"
#endif // MEASURE_TIME

using namespace std;

// This func is not designed to run in separate thread
// Because of passing by ref.
// If real object will be destroyed or corrupted that it will have affect on func arg
static int write_archive_to(const string& outname, const list<string>& files)
{
	struct archive *a;
	struct archive_entry *entry;
	struct stat st;
	char buff[8192];
	int len;
	int fd;

	a = archive_write_new();

	archive_write_add_filter_gzip(a);
	archive_write_set_format_pax_restricted(a);
	archive_write_open_filename(a, outname.c_str());
	for (auto f : files)
	{
		stat(f.c_str(), &st);
		entry = archive_entry_new();
		archive_entry_set_pathname(entry, f.c_str());
		archive_entry_set_size(entry, st.st_size);
		archive_entry_set_filetype(entry, AE_IFREG);
		archive_entry_set_perm(entry, 0644);
		archive_write_header(a, entry);
		fd = open(f.c_str(), O_RDONLY);
		len = read(fd, buff, sizeof(buff));
		while (len > 0) 
		{
			archive_write_data(a, buff, len);
			len = read(fd, buff, sizeof(buff));
		}
		close(fd);
		archive_entry_free(entry);
	}
	archive_write_close(a);
	archive_write_free(a);

	return 0;
}

// Pass by value to show that func can run in separate thread
// We work with copies, so we do not care what can happen with real objects
size_t list_dir_to_file(const string dir, const list<string> exts, const string outfile)
{
	size_t result = 0;
	boost::filesystem::path path = dir;
    boost::filesystem::recursive_directory_iterator itr(path);
    
    fstream fs(outfile, fstream::in | fstream::out | fstream::trunc);
    while (itr != boost::filesystem::recursive_directory_iterator())
    {
		if (boost::filesystem::is_regular_file(itr->status()) 
			&& find(begin(exts), end(exts), itr->path().extension().string()) != exts.end())
		{
			result++;
			fs << itr->path().string() << endl;
		}
        ++itr;
    }
    fs.close();
    return result;
}

// Pass by value to show that func can run in separate thread
// We work with copies, so we do not care what can happen with real objects
void tar_top_files(const string dir, const string outname)
{
	boost::filesystem::path path = dir;
    boost::filesystem::directory_iterator itr(path);
    list<string> files_for_tar;
    while (itr != boost::filesystem::directory_iterator())
    {
		if (boost::filesystem::is_regular_file(itr->status()))
		{
			files_for_tar.push_back(itr->path().string());
		}
        ++itr;
    }
    
    write_archive_to(outname, files_for_tar);
    cout << "Tar.gz creation finished " << endl;
}

int main()
{
	string dir_to_list = "/usr/include"; // experiment with "/home/vlad/Projects/CppMtLecture"
	string dir_to_tar = "/home/vlad/Downloads";
	string outfile = "list_out_file";
	
	list<string> exts = {".sh", ".cpp", ".h"};
	
	// tar -ztvf my_new_tar.tar.gz
	// to check what files are in archive
	string tar_out_name = "my_new_tar.tar.gz";
	
#ifdef MEASURE_TIME
	timer tm;
	tm.start();
#endif // MEASURE_TIME

#ifdef MT
	auto create_arch_th_ftr = async(std::launch::async, tar_top_files, dir_to_tar, tar_out_name);
#endif // MT

	try
	{
#ifndef MT
		tar_top_files(dir_to_tar, tar_out_name);
#endif // MT

		cout << "There are " << list_dir_to_file(dir_to_list, exts, outfile) << " files with given extensions" << endl;
		
#ifdef MT
		create_arch_th_ftr.get();
#endif // MT

	}
	catch(exception& e)
	{
		cout << "Caught exception: " << endl << e.what() << endl;
	}
	
#ifdef MEASURE_TIME
	cout << "Passed time: " << tm.get_duration(1).count() << endl;
#endif // MEASURE_TIME
    return 0;
}

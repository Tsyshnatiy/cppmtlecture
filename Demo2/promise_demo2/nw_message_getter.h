#pragma once

#include <string>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;
using namespace std;

// Classes cannot inherit from this class
class nw_message_getter final
{
private:
	boost::asio::io_service service;
	unsigned short port;
	static const int max_packet_len = 1024;
	
	string _get_message(tcp::socket && sock);
	
	// No copy
	nw_message_getter(const nw_message_getter&) = delete;
	// No move
	nw_message_getter(const nw_message_getter&&) = delete;
public:
	explicit nw_message_getter(unsigned short port);
	
	string get_message();
};

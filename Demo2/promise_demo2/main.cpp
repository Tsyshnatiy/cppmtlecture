#include <thread>		// thread
#include <stdexcept>	// exception demo
#include <iostream>		// cout
#include <future>		// future/promise

#include "nw_message_getter.h"

using namespace std;

// Gets message from client on port 5000
// After message received closes socket and returns message
void get_msg_from_client(promise<string> && prms)
{
	nw_message_getter nw_msg(5000);
	try
	{
		//throw std::runtime_error("ex");
		// Return result
		prms.set_value(nw_msg.get_message());
	}
	// Catch all exceptions
	catch (...)
	{
		// Capture exception
		prms.set_exception(current_exception());
	}
}


int main()
{
	// Create promise
	promise<string> prms;
	// Get future from it
	future<string> ftr = prms.get_future();
	
	// Signal to user we are alive
	cout << endl << "Starting get_message_thread" << endl;
	
	// Start server thread
	thread server_th(get_msg_from_client, move(prms));
	// We can do other job here.
	// Lets simply output something into cout
	cout << "Main thread execution continues" << endl;
	
	try
	{
		// Get server thread result.
		// get() returns immediately if set_value was called,
		// but waits for set_value if it was not called
		string message = ftr.get();
		cout << "Got message from client" << endl << message << endl;
	}
	catch (...)
	{
		cout << "Exception occured in server thread" << endl;
	}
	
	// Demonstrate wat happens if no join
	if (server_th.joinable())
	{
		server_th.join();
	}

	return 0;
}

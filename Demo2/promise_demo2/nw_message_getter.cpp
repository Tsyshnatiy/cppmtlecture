#include <string>
#include <boost/asio.hpp>

#include "nw_message_getter.h"

using namespace std;

nw_message_getter::nw_message_getter(unsigned short port)
{
	this->port = port;
}

string nw_message_getter::_get_message(tcp::socket && sock)
{
	char data[max_packet_len];
	boost::system::error_code error;
	size_t length = sock.read_some(boost::asio::buffer(data), error);
	if (error)
	{
		// Do not forget to close socket before throw
		sock.shutdown(boost::asio::ip::tcp::socket::shutdown_send, error);
		sock.close(error);
		throw boost::system::system_error(error);
	}
	
	sock.shutdown(boost::asio::ip::tcp::socket::shutdown_send, error);
	sock.close(error);
	
	return string(data, length);
}

string nw_message_getter::get_message()
{
	tcp::acceptor a(service, tcp::endpoint(tcp::v4(), port));
	tcp::socket sock(service);
	a.accept(sock);
	return _get_message(std::move(sock));
}

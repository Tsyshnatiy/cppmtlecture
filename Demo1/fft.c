#include <fftw3.h>
#include "fft.h"

int fft(double* r, double* im, const size_t len)
{
	fftw_complex *in, *out;
	fftw_plan p;
	int i = 0;
	
	in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * len);
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * len);
	
	for (i = 0 ; i < len ; ++i)
	{
		in[i][0] = r[i];
		in[i][1] = im[i];
	}
	
	p = fftw_plan_dft_1d(len, in, out, FFTW_FORWARD, FFTW_ESTIMATE);

	fftw_execute(p);

	fftw_destroy_plan(p);
	
	for (i = 0 ; i < len ; ++i)
	{
		r[i] = out[i][0] * len;
		im[i] = out[i][1] * len;
	}
	
	fftw_free(in); 
	fftw_free(out);
}

int ifft(double* r, double* im, const size_t len)
{
	fftw_complex *in, *out;
	fftw_plan p;
	int i = 0;
	
	in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * len);
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * len);
	
	for (i = 0 ; i < len ; ++i)
	{
		in[i][0] = r[i];
		in[i][1] = im[i];
	}
	
	p = fftw_plan_dft_1d(len, in, out, FFTW_BACKWARD, FFTW_ESTIMATE);

	fftw_execute(p);

	fftw_destroy_plan(p);
	
	for (i = 0 ; i < len ; ++i)
	{
		r[i] = out[i][0] * len;
		im[i] = out[i][1] * len;
	}
	
	fftw_free(in); 
	fftw_free(out);
}

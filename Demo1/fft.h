#ifndef FFT_H
#define FFT_H

#ifdef __cplusplus
extern "C"
{
#endif //__cplusplus

// Computes forward fft
// Places result to the place of input data
int fft(double* r, double* i, const size_t len);

// Computes inverse fft
// Places result to the place of input data
int ifft(double* r, double* i, const size_t len);

#ifdef __cplusplus
}
#endif //__cplusplus

#endif // FFT_H

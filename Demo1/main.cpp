#ifdef MT
#include <thread> 		// thread
#endif // MT

#include <memory> 		// shared_ptr
#include <cmath> 		// sqrt
#include <vector>		// vector

#ifdef SHOW_RESULTS
#include <fstream>		// fstream
#endif // SHOW_RESULTS

#include <cstdlib>		// malloc / free
#include <algorithm>	// transform
#include <list>			// list

#ifdef MEASURE_TIME
#include <iostream>		// cout
#include "timer.h"		// custom timer
#endif // MEASURE_TIME

#include "fft.h"		// fft

using namespace std;

// Counts number
static const size_t COUNTS_NUMBER = 5000000;

// Discretization frequency
static const double DISC_FREQ = 2048.0f;

// Discretization period
static const double DISC_PERIOD = 1.0f / DISC_FREQ;

// Nyquist frequency
static const double NYQIST_FREQ = DISC_FREQ / 2;

typedef list<pair<double, double>> extremum_list;

struct signal final
{
	vector<double> x;
	vector<double> y;
	size_t length = 0;
	
	signal(const size_t len)
	{
		// allocate memory
		x.reserve(len);
		y.reserve(len);
		
		length = len;
	}
};

// Generates
// 0.5 * cos(2 * pi * 50 * t) + cos(2 * pi * 120 * t)
static shared_ptr<signal> generate_signal(const double from, const double dt, const size_t len)
{
    shared_ptr<signal> s(new signal(len));
    double t = from;
    double two_pi = 2 * M_PI;
    
	for (auto i = 0 ; i < COUNTS_NUMBER ; ++i)
	{
		s->x[i] = t;
		s->y[i] = 0.5 * cos(two_pi * 50 * t) + cos(two_pi * 120 * t);
		t += dt;
	}
	
	return s; // Attention! No copy, move!
}

#ifdef SHOW_RESULTS

// Writes signal to .dat file
static void drop_signal_for_gnuplot(const shared_ptr<signal>& s, const string& fname)
{
	fstream fs(fname, fstream::in | fstream::out | fstream::trunc);
	
	// Write header
	fs << "#X\tY" << endl;
	
	// Write values
	for (auto i = 0 ; i < s->length ; ++i)
    {
		fs << s->x[i] << "\t" << s->y[i] << endl;
	}
	fs.close();
}

// Calls gnuplot for visualization
static void show_signal(const string& fname)
{
	// gnuplot cmd, interpolation with triangle convolution kernel
	auto cmd = "gnuplot -p -e \"plot '" + fname + "' with lines\"";
	system(cmd.c_str());
}

// Writes extremum list into file
static void drop_extremums_for_gnuplot(extremum_list& el, const string& fname)
{
	fstream fs(fname, fstream::in | fstream::out | fstream::trunc);
	
	// Write header
	fs << "#X\tY" << endl;
	
	for (auto e : el)
	{
		fs << e.first << "\t" << e.second << endl;
	}
	fs.close();
}

// Shows extremums plot
static void show_extremums(const string& fname)
{
	// gnuplot cmd, interpolation with triangle convolution kernel
	auto cmd = "gnuplot -p -e \"plot '" + fname + "'\"";
	system(cmd.c_str());
}
#endif // SHOW_RESULTS

// Computes abs(fft[sg])
// Sets result to signal_spectrum_abs.
// Result is half spectrum because sg assumed to be real valued
void compute_fourier(const shared_ptr<signal> sg, shared_ptr<signal>& signal_spectrum_abs)
{
    auto re = (double*) malloc(sizeof(double) * sg->length);
    auto im = (double*) malloc(sizeof(double) * sg->length);
    auto signal_len = sg->length;
    
	// assumed that signal is real valued
    fill(im, im + sg->length, 0);

	// normalize input values
	transform(sg->y.data(), sg->y.data() + signal_len, re,
		[&signal_len](double v) -> double { return v / signal_len; });
    
    // compute fft
    fft(re, im, signal_len);
    
    // allocate result
    signal_spectrum_abs.reset();
    signal_spectrum_abs = shared_ptr<signal>(new signal(signal_len / 2));
    
	// fill result with values
	for (auto i = 0 ; i < signal_spectrum_abs->length; ++i)
	{
		auto rei = re[i] / signal_len;
		auto imi = im[i] / signal_len;
		
		// generate frequency range
		signal_spectrum_abs->x[i] = NYQIST_FREQ * i / signal_spectrum_abs->length;
		
		// generate spectrum abs values
		signal_spectrum_abs->y[i] = 2 * sqrt(rei * rei + imi * imi);
	}
    
    free(re);
    free(im);
}

extremum_list compute_extremums(const shared_ptr<signal> sg)
{
	extremum_list result;
	for (int i = 1 ; i < sg->length - 1 ; ++i)
	{
		if ((sg->y[i] >= sg->y[i - 1] && sg->y[i] > sg->y[i + 1])
			|| (sg->y[i] > sg->y[i - 1] && sg->y[i] >= sg->y[i + 1])
			|| (sg->y[i] <= sg->y[i - 1] && sg->y[i] < sg->y[i + 1])
			|| (sg->y[i] < sg->y[i - 1] && sg->y[i] <= sg->y[i + 1]))
        {
			result.push_back(make_pair(sg->x[i], sg->y[i]));
		}
	}
	
	return result;
}

int main()
{
#ifdef SHOW_RESULTS	
	// filenames for gnuplot data
	static const string signal_dat_fname("signal.dat");
	static const string signal_spectrum_dat_fname("signal_spectrum.dat");
	static const string signal_extremums_fname("signal_extremums.dat");
#endif // SHOW_RESULTS

	// signal generation
	auto garmonic_signal = generate_signal(0, DISC_PERIOD, COUNTS_NUMBER);
	
	// spectrum signal initialization
	shared_ptr<signal> garmonic_signal_spectrum_abs(nullptr);

#ifdef MEASURE_TIME
	timer tim;
	tim.start();
#endif // MEASURE_TIME

#ifdef MT
	// start thread that computes fft
    thread fft_thread(compute_fourier, garmonic_signal, ref(garmonic_signal_spectrum_abs));
#else  
	compute_fourier(garmonic_signal, garmonic_signal_spectrum_abs);
#endif // MT

	auto extremums = compute_extremums(garmonic_signal);

#ifdef MT
	// join fft thread
    if (fft_thread.joinable())
    {
		fft_thread.join();
	}
#endif // MT

#ifdef MEASURE_TIME
	cout << "Elapsed time is " << tim.get_duration(1).count() << endl;
#endif // MEASURE_TIME

#ifdef SHOW_RESULTS	
    // Write signals to file
    drop_extremums_for_gnuplot(extremums, signal_extremums_fname);
    drop_signal_for_gnuplot(garmonic_signal, signal_dat_fname);
	drop_signal_for_gnuplot(garmonic_signal_spectrum_abs, signal_spectrum_dat_fname);

	// show original signal
	show_signal(signal_dat_fname);
	
	// show signal spectrum
	show_signal(signal_spectrum_dat_fname);
	
	show_extremums(signal_extremums_fname);
#endif // SHOW_RESULTS

	return 0;
}

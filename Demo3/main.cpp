#include <iostream>
#include <fstream>
#include <cfloat> 
#include <random>
#include <memory>

#include "tbb/blocked_range.h"
#include "tbb/parallel_reduce.h"

#include "timer.h"

using namespace tbb;
using namespace std;

typedef uint64_t very_long;

#define CURVE

class MinIndexFoo 
{
private:
	const float *const my_a;
public:
	float value_of_min;
	long index_of_min;
	
	void operator()(const blocked_range<size_t>& r)
	{
		const float *a = my_a;
		for (size_t i = r.begin() ; i != r.end() ; ++i)
		{
			float value = a[i];
			if (value < value_of_min)
			{
				value_of_min = value;
				index_of_min = i;
			}
		}
	}
	
	MinIndexFoo(MinIndexFoo& x, split):
		my_a(x.my_a), value_of_min(FLT_MAX), index_of_min(-1)
	{}  
	
	void join(const MinIndexFoo& y) 
	{
		if (y.value_of_min < value_of_min)
		{
			value_of_min = y.value_of_min;
			index_of_min = y.index_of_min;
		}
	}
	
	MinIndexFoo(const float a[]):
		my_a(a), value_of_min(FLT_MAX), index_of_min(-1)
	{} 
};

long ParallelMinIndexFoo(float a[], size_t n, size_t grain_size)
{
	MinIndexFoo mif(a);
	// Grainsize is equal to n / number_of_threads
	// We use simple_partitioner
	// Hence number of operations per thread will be grain_size / 2 <= ops <= grain_size
	parallel_reduce(blocked_range<size_t>(0, n, grain_size), mif, simple_partitioner());
	return mif.index_of_min;
} 

int main(int argc, char* argv[])
{
	const very_long data_size = 1000000;
	very_long idx = -1;
    auto data = new float[data_size];

#ifdef CURVE
	string curve_fname = "curve.dat";
#endif // CURVE

	std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> distrib(1, 60000);
    
    // look here! Magic happens!)
    auto wisdom = std::bind(distrib, gen);
	
    for (very_long i = 0; i < data_size; ++i) 
    {
		// Ask what difference btw *(data + i) and data[i] or i[data]
        //*(data + i) = wisdom();
        i[data] = wisdom();
        //cout << i[data] << " ";
    }
    
    timer tm;
 
#ifndef CURVE  
#ifdef MT
	tm.start();
	idx = ParallelMinIndexFoo(data, data_size, 10);
#else
	float val = data[0];
	for (very_long i = 0; i < data_size; ++i) 
    {
		if (data[i] < val)
		{
			idx = i;
			val = data[i];
		}
    }
#endif // MT
	cout << "Passed time is " << tm.get_duration(1).count() << " seconds" << endl;
    cout << "Minimal index is: " << idx << endl;
#else // if CURVE defined
	// Create gnuplot file
	int i = 1;
	fstream fs(curve_fname, fstream::in | fstream::out | fstream::trunc);
	
	// Write header
	fs << "#X\tY" << endl;
	
	// Do many experiments with different grainsize
	while (i < 100000)
	{
		tm.start();
		
		ParallelMinIndexFoo(data, data_size, 10 * i);
		
		// Measure each experiment time
		auto seconds = tm.get_duration(1).count();
		
		fs << 10 * i << "\t" << seconds << endl;
		i++;
	}
	fs.close();
	
	// Call gnuplot
	auto cmd = "gnuplot -p -e \"plot '" + curve_fname + "' with lines\"";
	system(cmd.c_str());
#endif // CURVE

    delete [] data;
    
	return 0;
}
